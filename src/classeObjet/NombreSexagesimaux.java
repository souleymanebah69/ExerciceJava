package classeObjet;

public class NombreSexagesimaux {
	private int heure , min , sec;

	public NombreSexagesimaux(int heure, int min, int sec) {
	
		this.heure = heure;
		this.min = min;
		this.sec = sec;
	
	}
	
	public NombreSexagesimaux(double dec) {
		 
		heure = (int)dec;
		int minDec = (int)(60*(dec - heure));
		min = (int)minDec;
		sec = (int)(60 *(minDec-min));
		
	}
	
	public int getDec () {
		return (3600*heure +60*min + sec)/3600;
	}
	
	public int getH () {
		return heure;
	}
	 public int getM() {
		 return min;
	 }
	
	 public int getS() {
		 return sec;
	 }

}
