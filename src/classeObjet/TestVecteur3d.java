package classeObjet;

public class TestVecteur3d {

	public static void main(String[] args) {

		Vecteur3d v1 = new Vecteur3d(3,2,5);
		Vecteur3d v2 = new Vecteur3d(1, 2,3);
		System.out.print("v1 = " ); v1.affichage3d();
		System.out.print("v2 = "); v2.affichage3d();
		Vecteur3d som ;
		   som = Vecteur3d.sommeVecteur(v1, v2);
		   System.out.print("v1 + v2 = " ); som.affichage3d();
		   System.out.print("v1.v2 = " + v1.produitScaleurVecteur(v2)); 
		
	}

}
