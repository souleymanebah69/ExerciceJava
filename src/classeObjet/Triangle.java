package classeObjet;

import java.util.Scanner;

public class Triangle {

	public static void main(String[] args) {
		int nLignes ; // nombre de lignes
		int numLigne ; // compteur de ligne
		int nbEspaces ;  // nombre d'espaces pr�cedent une �toile
		final char cRempli = '*'; // caract�re de remplissage 
		int j;
		Scanner clavier = new Scanner(System.in);

		System.out.println("Entrez le nombre de ligne " );
		nLignes = clavier.nextInt();
		clavier.nextLine();
		for(numLigne = 0 ; numLigne < nLignes; numLigne++) {
			nbEspaces = nLignes - numLigne - 1;
			for(j = 0; j < nbEspaces ; j++) {
				System.out.print(" ");
			
			}
			for(j = 0 ; j < 2*numLigne + 1 ; j++) {
				System.out.print(cRempli);
			}
			
			System.out.println();

		}
		System.out.println();

		// Triangle rectangle 
		for(numLigne = 0 ; numLigne < nLignes; numLigne++) {
			for(j = 0; j < numLigne + 1; j++) {
				if(j == numLigne) {
					System.out.print("'");
				}else {
					System.out.print(cRempli);

				}
			}
			System.out.println();
		}
		
	}

}
