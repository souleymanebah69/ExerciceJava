package classeObjet;

public class Vecteur3d {
	private double x , y , z;

	public Vecteur3d(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public void affichage3d() {	
		System.out.println("< "+x +", "+ y + "," + z +" >");
	}
	
	public double norme() {
		return (Math.sqrt(x*x + y*y + z*z));
	}
	
	public static Vecteur3d sommeVecteur(Vecteur3d v , Vecteur3d w) {
		
		Vecteur3d s = new Vecteur3d(0, 0, 0);
		s.x = v.x + w.x ; s.y= v.y + w.y ; s.z= v.z + w.z;
		return s;	
	}
	public double produitScaleurVecteur(Vecteur3d v) {
		return (x*v.x + y*v.y + z*v.z);
	}
	
	
}
